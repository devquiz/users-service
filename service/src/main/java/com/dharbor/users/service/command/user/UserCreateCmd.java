package com.dharbor.users.service.command.user;

import com.dharbor.users.api.input.UserInput;
import com.dharbor.users.service.model.domain.User;
import com.dharbor.users.service.model.repositories.UserRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Alejandro R
 */
@SynchronousExecution
public class UserCreateCmd implements BusinessLogicCommand {

    @Setter
    private UserInput input;

    @Getter
    private User user;

    @Autowired
    private UserRepository repository;

    @Override
    public void execute() {
        user = repository.save(composeUserInstance(input));
    }

    private User composeUserInstance(UserInput input) {
        User instance = new User();
        instance.setAccountId(input.getAccountId());
        instance.setFirstName(input.getFirstName());
        instance.setLastName(input.getLastName());

        return instance;
    }
}
