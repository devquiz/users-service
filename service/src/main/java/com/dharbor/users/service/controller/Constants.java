package com.dharbor.users.service.controller;

/**
 * @author Alejandro R
 */
public class Constants {
    private Constants() {

    }

    public static class BasePath {
        public static final String SECURE = "/secure";
        public static final String SECURE_USERS = SECURE + "/users";
    }

    public static class UsersTag {
        public static final String NAME = "Users";
    }
}
