package com.dharbor.users.service.model.repositories;

import com.dharbor.users.service.model.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Alejandro R
 */
public interface UserRepository extends JpaRepository<User, Long> {

}
