package com.dharbor.users.service.model.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Alejandro R
 */

@Setter
@Getter
@Entity
@Table(name = "user_table")
@Inheritance(strategy = InheritanceType.JOINED)
public class User {

    @Id
    @Column(name = "userid", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "accountid", unique = true, nullable = false)
    private Long accountId;

    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Column(name = "lastname", nullable = false)
    private String lastName;

    @Column(name = "createddate", nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "isdeleted", nullable = false)
    private Boolean isDeleted;

    @PrePersist
    void onPrePersist() {
        this.createdDate = new Date();
        this.isDeleted = false;
    }
}
