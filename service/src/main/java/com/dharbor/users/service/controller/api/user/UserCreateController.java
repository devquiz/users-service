package com.dharbor.users.service.controller.api.user;

import com.dharbor.users.api.input.UserInput;
import com.dharbor.users.api.response.UserResponse;
import com.dharbor.users.service.command.user.UserCreateCmd;
import com.dharbor.users.service.controller.Constants;
import com.dharbor.users.service.model.builder.UserResponseBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Alejandro R
 */
@Api(
        tags = Constants.UsersTag.NAME
)
@RequestMapping(value = Constants.BasePath.SECURE_USERS)
@RequestScope
@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class UserCreateController {

    @Autowired
    private UserCreateCmd userCreateCmd;

    @ApiOperation(value = "Create a user")
    @PostMapping
    public UserResponse createUser(@RequestBody UserInput input) {
        userCreateCmd.setInput(input);
        userCreateCmd.execute();

        return UserResponseBuilder.getInstance(userCreateCmd.getUser()).build();
    }

}
