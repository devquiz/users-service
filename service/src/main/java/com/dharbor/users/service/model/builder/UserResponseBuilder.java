package com.dharbor.users.service.model.builder;

import com.dharbor.users.api.response.UserResponse;
import com.dharbor.users.service.model.domain.User;

/**
 * @author Alejandro R
 */
public class UserResponseBuilder {

    private UserResponse instance;

    public static UserResponseBuilder getInstance(User user) {
        return (new UserResponseBuilder()).setUser(user);
    }

    private UserResponseBuilder() {
        this.instance = new UserResponse();
    }

    private UserResponseBuilder setUser(User user) {
        instance.setAccountId(user.getAccountId());
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());
        instance.setIsDeleted(user.getIsDeleted());
        instance.setCreatedDate(user.getCreatedDate());

        return this;
    }

    public UserResponse build() {
        return instance;
    }
}
