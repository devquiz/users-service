package com.dharbor.users.api.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author Alejandro R
 */
@Getter
@Setter
public class UserResponse {

    private Long accountId;

    private String firstName;

    private String lastName;

    private Boolean isDeleted;

    private Date createdDate;
}
