package com.dharbor.users.api.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Alejandro R
 */
@Getter
@Setter
public class UserInput {

    private Long accountId;

    private String firstName;

    private String lastName;

    public String getFirstName() {
        return this.firstName;
    }
}